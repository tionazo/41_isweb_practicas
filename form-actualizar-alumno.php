<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Modificación datos de alumno</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<?php
    session_start();

    $expediente = $_SESSION['expediente'];
    $nombre     = $_SESSION['nombre'];
    $usuario    = $_SESSION['usuario'];
    $clave      = $_SESSION['clave'];
    $f_nac      = $_SESSION['f_nac'];
    $origen     = $_SESSION['origen'];
    $email      = $_SESSION['email'];
    $observaciones = $_SESSION['observaciones'];

 ?>

    <h1>Formulario de MODIFICACIÓN de datos del alumno: <?php echo $nombre; ?></h1>
    <form action="modificar-usario.php" method="post">
        <p>
            <label for="expediente">Expediente:  </label> <b><?php echo $_SESSION['expediente']; ?></b>
        </p>
        <p>
            <label for="nombre">Nombre:  </label> <br/>
            <input type="text" name="nombre" size="20" value="<?php echo $nombre; ?>" required />
        </p>
        <p>
            <label for="usuario">Usuario:  </label> <br/>
            <input type="text" name="usuario" size="10" value="<?php echo $usuario; ?>" required />
        </p>
        <p>
            <label for="password">Clave:  </label> <br/>
            <input type="password" name="password" size="8" value="<?php echo $clave; ?>" required />
        </p>
        <p>
            <label for="f_nac">Fecha nacimiento:  </label> <br/>
            <input type="date" name="f_nac" value="<?php echo $f_nac; ?>" required />
        </p>
<!--        <p>
            <label for="origen">Origen:  </label> <br/>
            <input type="radio" name="origen" value="local">Local
            <input type="radio" name="origen" value="regional">Regional
            <input type="radio" name="origen" value="nacional">Nacional
            <input type="radio" name="origen" value="extranjero">Extranjero
        </p>
-->
        <p>
            <label for="origen">Origen:  </label> <br/>
            <select name="origen" id="origen">
                <option value="local">Local</option>
                <option value="regional">Regional</option>
                <option value="nacional">Nacional</option>
                <option value="extranjero">Extranjero</option>
            </select>
        </p>
        <p>
            <label for="email">email:  </label> <br/>
            <input type="email" name="email" size="40" value="<?php echo $email; ?>" required />
        </p>
        <p>
            <label for="observaciones">Observaciones:  </label> <br/>
            <textarea name="observaciones" rows="6" cols="80"><?php echo $observaciones; ?></textarea>
            </p>
        <p>
            <button class="submit" type="submit">Modificar</button>
        </p>
    </form>


<nav>
    <ul class="nav navbar-nav">
        <li><a href="ver-calificaciones.php">Mostrar calificaciones</a></li>
        <li><a href="form-actualizar-alumno.php">Actualizar datos alumno</a></li>
        <li><a href="eliminar-alumno.php">Eliminar alumno</a></li>
        <li><a href="calificar-alumno.php">Calificar alumno</a></li>
        <li><a href="lista-alumnos.php">LISTADO ALUMNOS</a></li>
    </ul>
</nav>

</body>
</html>
