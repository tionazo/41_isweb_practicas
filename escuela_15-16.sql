-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-11-2015 a las 15:58:38
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `escuela_15-16`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `expediente` char(5) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `usuario` varchar(10) NOT NULL,
  `clave` varchar(8) NOT NULL,
  `f_nac` date NOT NULL,
  `origen` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `observaciones` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`expediente`, `nombre`, `usuario`, `clave`, `f_nac`, `origen`, `email`, `observaciones`) VALUES
('00001', 'Rodrigo', 'rodri', 'rodri', '2005-11-08', 'Zamora', 'rodri@gmail.com', 'Esto son las observaciones acerca de Rodrigo'),
('00002', 'Gonzalo', 'gonza', 'gonza', '2004-10-07', 'Salamanca', 'gonza@gmail.com', 'Esto son las observaciones acerca de Gonzalo'),
('00003', 'Francisco', 'ilde', 'ilde', '2003-09-01', 'Palencia', 'ilde@gmail.com', 'Esto son las observaciones acerca de Francisco alias Ilde'),
('00004', 'Jaime', 'jaime', 'jaime', '2002-07-18', 'Bejar', 'jaime@gmail.com', 'Esto son las observaciones acerca de Jaime'),
('00005', 'Antonio', 'tono', 'tono', '2005-04-15', 'Caceres', 'tono@gmail.com', 'Esto son las observaciones acerca de Antonio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaturas`
--

CREATE TABLE IF NOT EXISTS `asignaturas` (
  `cod_asig` int(10) NOT NULL,
  `nomb_asig` varchar(20) NOT NULL,
  `creditos` int(10) NOT NULL,
  `curso` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignaturas`
--

INSERT INTO `asignaturas` (`cod_asig`, `nomb_asig`, `creditos`, `curso`) VALUES
(1, 'Ing Software Web', 6, 4),
(2, 'Sistemas Información', 6, 4),
(3, 'IPO', 6, 2),
(4, 'SSBBDD2', 6, 3),
(5, 'SSBBDD1', 6, 2),
(6, 'Algebra', 6, 1),
(7, 'Estadistica', 6, 3),
(8, 'I.A.', 6, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas`
--

CREATE TABLE IF NOT EXISTS `matriculas` (
  `expediente` char(5) NOT NULL,
  `cod_asig` int(10) NOT NULL,
  `calificaciones` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matriculas`
--

INSERT INTO `matriculas` (`expediente`, `cod_asig`, `calificaciones`) VALUES
('00001', 1, '5.00'),
('00001', 2, '5.50'),
('00002', 1, '8.00'),
('00002', 4, '7.50');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
 ADD PRIMARY KEY (`expediente`);

--
-- Indices de la tabla `asignaturas`
--
ALTER TABLE `asignaturas`
 ADD PRIMARY KEY (`cod_asig`);

--
-- Indices de la tabla `matriculas`
--
ALTER TABLE `matriculas`
 ADD PRIMARY KEY (`expediente`,`cod_asig`), ADD KEY `cod_asig` (`cod_asig`), ADD KEY `expediente` (`expediente`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `matriculas`
--
ALTER TABLE `matriculas`
ADD CONSTRAINT `matriculas_ibfk_1` FOREIGN KEY (`expediente`) REFERENCES `alumnos` (`expediente`),
ADD CONSTRAINT `matriculas_ibfk_2` FOREIGN KEY (`cod_asig`) REFERENCES `asignaturas` (`cod_asig`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
