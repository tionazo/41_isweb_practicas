<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Calificaciones alumno</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>

<?php
	session_start();

	require('config.php');
	require('conexion.php');

	$expediente = $_SESSION['expediente'];
	$nombre     = $_SESSION['nombre'];
	$usuario    = $_SESSION['usuario'];
	$clave      = $_SESSION['clave'];
	$f_nac      = $_SESSION['f_nac'];
	$origen     = $_SESSION['origen'];
	$email      = $_SESSION['email'];
	$observaciones = $_SESSION['observaciones'];

	echo "<h1>Calificaciones alumno: " . $nombre . "</h1>";

	$query = "SELECT expediente, cod_asig, calificaciones FROM matriculas WHERE expediente='" . $expediente . "' ";
	//echo $query;

	$consulta = mysql_query ($query, $conexion) or die ("Fallo en la consulta");

	$numeroRegistros = mysql_num_rows($consulta);
	//echo "numero registros" . $numeroRegistros;

if( $numeroRegistros ) {

$cabeceraTablaCalificaciones=<<<CABECERATABLACALIFICACIONES
<table>
	<tr>
		<th>expediente</th>
		<th>nombre</th>
		<th>cod_asig</th>
		<th>nom_asig</th>
		<th>calificaciones</th>
	</tr>
CABECERATABLACALIFICACIONES;

print $cabeceraTablaCalificaciones;

    for ($i=0; $i<$numeroRegistros; $i++) { 
        $fila = mysql_fetch_array ($consulta);
        $expediente = $fila['expediente'];
        $nombre     = $_SESSION['nombre'];
        $cod_asig   = $fila['cod_asig'];

        include 'datos-asignatura.php';

        $calificaciones = $fila['calificaciones'];

$filaTablaDatosAlumno=<<<FILATABLADATOSALUMNO
    <tr>
        <td>$expediente</td>
        <td>$nombre</td>
        <td>$cod_asig</td>
        <td>$nom_asig</td>
        <td>$calificaciones</td>
    </tr>
FILATABLADATOSALUMNO;

        print $filaTablaDatosAlumno;
    }



$pieTablaCalificaciones=<<<PIETABLACALIFICACIONES

</table>

PIETABLACALIFICACIONES;
    print $pieTablaCalificaciones;

} else{
    $mensajeError = "No hay calificaciones registradas";
    echo "<script type='text/javascript'>alert('$mensajeError');</script>";
    //sleep(5);
    //header ("Location: index.html");
}


?>



<nav>
    <ul class="nav navbar-nav">
        <li><a href="ver-calificaciones.php">Mostrar calificaciones</a></li>
        <li><a href="form-actualizar-alumno.php">Actualizar datos alumno</a></li>
        <li><a href="eliminar-alumno.php">Eliminar alumno</a></li>
        <li><a href="calificar-alumno.php">Calificar alumno</a></li>
        <li><a href="lista-alumnos.php">LISTADO ALUMNOS</a></li>
    </ul>
</nav>

</body>
</html>

