<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<h1>Listado de todos los alumnos</h1>

<?php
session_start();

$user     = $_POST["user"];
$password = $_POST["password"];

require('config.php');
require('conexion.php');

$query = "SELECT expediente, nombre, usuario, clave, f_nac, origen, email, observaciones FROM alumnos";
//echo $query;

$consulta = mysql_query ($query, $conexion) or die ("Fallo en la consulta");

$numeroRegistros = mysql_num_rows($consulta);

echo "<h2>Numero de alumnos:" . $numeroRegistros .  " </h2>";

if( $numeroRegistros ) {

$cabeceraTablaDatosAlumnos=<<<CABECERATABLADATOSALUMNOS
<table>
    <tr>
        <th>expediente</th>
        <th>nombre</th>
        <th>usuario</th>
        <th>clave</th>
        <th>f_nac</th>
        <th>origen</th>
        <th>email</th>
        <th>observaciones</th>
    </tr>
CABECERATABLADATOSALUMNOS;

print $cabeceraTablaDatosAlumnos;


    for ($i=0; $i<$numeroRegistros; $i++) { 
        $fila = mysql_fetch_array ($consulta); 
        $expediente = $fila['expediente'];
        $nombre     = $fila['nombre'];
        $usuario    = $fila['usuario'];
        $clave      = $fila['clave'];
        $f_nac      = $fila['f_nac'];
        $origen     = $fila['origen'];
        $email      = $fila['email'];
        $observaciones = $fila['observaciones'];

$filaTablaDatosAlumno=<<<FILATABLADATOSALUMNO
    <tr>
        <td>$expediente</td>
        <td>$nombre</td>
        <td>$usuario</td>
        <td>$clave</td>
        <td>$f_nac</td>
        <td>$origen</td>
        <td>$email</td>
        <td>$observaciones</td>
    </tr>
FILATABLADATOSALUMNO;

        print $filaTablaDatosAlumno;
    }

$pieTablaDatosAlumno=<<<PIETABLADATOSALUMNO

</table>

PIETABLADATOSALUMNO;
    print $pieTablaDatosAlumno;

} else{
    $mensajeError = "No hay alumnos registrados";
    echo "<script type='text/javascript'>alert('$mensajeError');</script>";
    //sleep(5);
    //header ("Location: index.html");
}

?>

</body>
</html>

