

<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="./" class="navbar-brand">WebApp Alumno</a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li><a href="ver-calificaciones.php">Mostrar calificaciones</a></li>
        <li><a href="form-actualizar-alumno.php">Actualizar datos alumno</a></li>
        <li><a href="eliminar-alumno.php">Eliminar alumno</a></li>
        <li><a href="calificar-alumno.php">Calificar alumno</a></li>
        <li><a href="lista-alumnos.php">LISTADO ALUMNOS</a></li>
      </ul>
    </nav>
  </div>
</header>
