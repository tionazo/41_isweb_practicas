<?php

session_start();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Información del alumno</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<?php // include 'nav.php'; ?>

<h1>Información del alumno</h1>

<?php

$expediente = $_SESSION['expediente'];
$nombre     = $_SESSION['nombre'];
$usuario    = $_SESSION['usuario'];
$clave      = $_SESSION['clave'];
$f_nac      = $_SESSION['f_nac'];
$origen     = $_SESSION['origen'];
$email      = $_SESSION['email'];
$observaciones = $_SESSION['observaciones'];

$tablaDatosAlumno=<<<TABLADATOSALUMNO
<table>
    <tr>
        <th>expediente</th>
        <th>nombre</th>
        <th>usuario</th>
        <th>clave</th>
        <th>f_nac</th>
        <th>origen</th>
        <th>email</th>
        <th>observaciones</th>
    </tr>
    <tr>
        <td>$expediente</td>
        <td>$nombre</td>
        <td>$usuario</td>
        <td>$clave</td>
        <td>$f_nac</td>
        <td>$origen</td>
        <td>$email</td>
        <td>$observaciones</td>
    </tr>
</table>
TABLADATOSALUMNO;

print $tablaDatosAlumno;

?>


<nav>
    <ul class="nav navbar-nav">
        <li><a href="ver-calificaciones.php">Mostrar calificaciones</a></li>
        <li><a href="form-actualizar-alumno.php">Actualizar datos alumno</a></li>
        <li><a href="eliminar-alumno.php">Eliminar alumno</a></li>
        <li><a href="calificar-alumno.php">Calificar alumno</a></li>
        <li><a href="lista-alumnos.php">LISTADO ALUMNOS</a></li>
    </ul>
</nav>

</body>
</html>

